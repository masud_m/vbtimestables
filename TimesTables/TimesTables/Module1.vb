﻿Module Module1

    'get the documents folder and make a file
    Dim file As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\TimesTablesOutput.txt"

    'overwrite or append
    Dim saveFile As String = "append"

    Sub Main()
        'show the menu
        displayMenu()

        'prevent the console terminating quickly
        Console.ReadLine()
    End Sub

    'display the menu
    Sub displayMenu()
        Dim userInput As Integer 'will hold a value depending on user input

        'print to the console: the menu
        Console.WriteLine("What would you like to do?")
        Console.WriteLine("[1] Generate times tables")
        Console.WriteLine("[2] Overwrite or append (if exists) file. Currently " &
                          saveFile & " at location " & file)
        Console.WriteLine("[3] Quit")

        userInput = Val(Console.ReadLine()) 'this converts as much of input into int

        Select Case userInput
            Case 1
                'generate times tables
                generateTimesTables()
            Case 2
                'change the output
                changeOutput()
            Case 3
                'quit the application
                End
            Case Else
                'they typed something else (not 1, 2 or 3), so return to the main menu
                Console.WriteLine("Please select one of the option.")
                displayMenu()
        End Select
    End Sub

    'get their input and generate times tables
    Sub generateTimesTables()
        'var for holding their input
        Dim num As Integer

        Console.WriteLine("Which number would you like times tables for? (between 1 and 12)")

        'get their input
        num = Val(Console.ReadLine)

        'if it is more than 12 or less than 1....
        If (num < 1 Or num > 12) Then
            Console.WriteLine("Only numbers between 1 and 12.")
            'run the sub again (loop)
            generateTimesTables()
        Else
            'get ready to output file - this will hold the steamwriter
            Dim objWriter

            'create a new streamwriter to manipulate files
            Select Case saveFile
                Case "append"
                    'if they want to append...
                    objWriter = New System.IO.StreamWriter(file, True)
                Case "overwrite"
                    'if they want to overwrite...
                    objWriter = New System.IO.StreamWriter(file, False)
                Case Else
                    'otherwise (this should not happen, but just in case)
                    Console.WriteLine("Error.")
                    changeOutput()
            End Select

            'if it is valid, continue on
            'loop from 1 to 12 (inclusive) and print the answer
            For i = 1 To 12
                'print the answer
                Console.WriteLine(num & " times " & i & " is " & num * i)
                'write to the file
                objWriter.WriteLine(num & " times " & i & " is " & num * i)
            Next

            'close the steamwriter to prevent any memory leaks, etc.
            objWriter.Close()
        End If

        'wait for them to continue
        Console.ReadLine()

        'afterwards, go back to the main menu...
        Main()
    End Sub

    Sub changeOutput()
        'if the file already exists...
        If System.IO.File.Exists(file) = True Then
            'a var to hold user input
            Dim userInput As Integer

            'provide the user some options...
            Console.WriteLine("Would you like to:")
            Console.WriteLine("[1] Append OR")
            Console.WriteLine("[2] Overwrite")

            userInput = Val(Console.ReadLine) 'convert input to int

            'depending on input...
            Select Case userInput
                Case 1
                    'append the file
                    saveFile = "append"
                Case 2
                    'overwrite the file
                    saveFile = "overwrite"
                Case Else
                    'they selected non-existent option, try again
                    Console.WriteLine("Unknown option - try again.")
                    changeOutput()
            End Select

            'display their action and where 
            Console.WriteLine(saveFile & " to " & file)
        Else
            'the file does not exist
            Console.WriteLine("File does not exist. Writing to new file at " & file)
        End If

        'wait for user
        Console.ReadLine()

        'return to main menu
        Main()
    End Sub

End Module
